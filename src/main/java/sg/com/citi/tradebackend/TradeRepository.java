package sg.com.citi.tradebackend;

import org.springframework.data.jpa.repository.JpaRepository;
import sg.com.citi.tradebackend.model.Trade;
import sg.com.citi.tradebackend.model.TradeState;
import sg.com.citi.tradebackend.model.TradeType;

import java.util.List;

public interface TradeRepository extends JpaRepository<Trade, Integer> {

    List<Trade> findByType(TradeType tradeType);
    List<Trade> findByState(TradeState tradeState);
}
