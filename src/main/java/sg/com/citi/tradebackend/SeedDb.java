package sg.com.citi.tradebackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import sg.com.citi.tradebackend.model.TradeState;
import sg.com.citi.tradebackend.model.TradeType;

import javax.sql.DataSource;
import java.util.Date;

@Component
public class SeedDb {

    @Autowired
    JdbcTemplate template;

    @Autowired
    public void SeedDb(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(
                "insert into trade (created, state, type, exchange, ticker, quantity, price) values (?, ?, ?, ?, ?, ?, ?)",
                new Object[]{new Date(System.currentTimeMillis()), TradeState.CREATED.getState(), TradeType.BUY.getTradeType(), "NYSE", "GOOG", 1.00, 1.00});
    }
}