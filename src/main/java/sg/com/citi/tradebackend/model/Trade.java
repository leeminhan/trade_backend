package sg.com.citi.tradebackend.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter @Setter @NoArgsConstructor
public class Trade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date created = new Date(System.currentTimeMillis());

    @Enumerated(EnumType.STRING)
    private TradeState state = TradeState.CREATED;

    @Enumerated(EnumType.STRING)
    private TradeType type = TradeType.BUY;

    private String exchange;
    private String ticker;
    private double quantity;
    private double price;

    public Trade(Date created, TradeState state, TradeType type, String exchange, String ticker, double quantity, double price) {
        this.created = created;
        this.state = state;
        this.type = type;
        this.exchange = exchange;
        this.ticker = ticker;
        this.quantity = quantity;
        this.price = price;
    }
}
