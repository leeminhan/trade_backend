package sg.com.citi.tradebackend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sg.com.citi.tradebackend.model.Trade;
import sg.com.citi.tradebackend.model.TradeState;
import sg.com.citi.tradebackend.model.TradeType;

import java.util.List;

@RestController
@CrossOrigin
@Slf4j
@RequestMapping("/trades")
public class TradeRestController {

    @Autowired
    private TradeService tradeService;

    @GetMapping()
    @ResponseBody
    public ResponseEntity<List<Trade>> getAllTradeRecords() {
        List<Trade> result = tradeService.getAllTradeRecords();
        if (result == null)
            return ResponseEntity.notFound().build();
        else
            return ResponseEntity.ok().body(result);
    }

    @GetMapping(value = "type/{tradeType}")
    @ResponseBody
    public ResponseEntity<List<Trade>> getTradesByTradeType(@PathVariable String tradeType) {
        TradeType tradeTypeObj = TradeType.valueOf(tradeType.toUpperCase());
        List<Trade> result = tradeService.getTradesByTradeType(tradeTypeObj);
        if (result == null)
            return ResponseEntity.notFound().build();
        else
            return ResponseEntity.ok().body(result);
    }

    @GetMapping(value = "state/{tradeState}")
    @ResponseBody
    public ResponseEntity<List<Trade>> getTradesByTradeState(@PathVariable String tradeState) {
        TradeState tradeStateObj = TradeState.valueOf(tradeState.toUpperCase());
        List<Trade> result = tradeService.getTradesByTradeState(tradeStateObj);
        if (result == null)
            return ResponseEntity.notFound().build();
        else
            return ResponseEntity.ok().body(result);
    }

    @PostMapping()
    public ResponseEntity<String> addTradeRecord(@RequestBody Trade newTrade){
        if (newTrade == null)
            return ResponseEntity.notFound().build();
        else
            tradeService.addTradeRecord(newTrade);
        return new ResponseEntity<String>("Add New Trade Record Successful", HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateTradeRecordByTradeId(@PathVariable int id, @RequestBody Trade newTrade){
        if (newTrade == null)
            return ResponseEntity.notFound().build();
        else
            tradeService.updateTradeRecordById(id, newTrade);
        return new ResponseEntity<String>("Update Trade Record Successful", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<String> deleteTradeRecordByTradeId(@PathVariable int id){
        tradeService.deleteTradeRecord(id);
        return new ResponseEntity<String>("DELETE by Trade Record Id Successful", HttpStatus.OK);
    }
}
